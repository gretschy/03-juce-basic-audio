/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    
    if (message.isNoteOn())
    {
        sineOscillate.setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
        DBG("Note number: " << message.getNoteNumber() << "\n");
        
        sineOscillate.setAmplitude(message.getVelocity()/127.0);
        DBG("Velocity: " << message.getVelocity()/127.0 << "\n");
    }

    else if (message.isNoteOff())
        {
            sineOscillate.setAmplitude(0.0);
        }
    

}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        float sineWave = sineOscillate.getSample();
        
        *outL = sineWave;
        *outR = sineWave;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}

void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    DBG("Device About to Start");
    sineOscillate.setSample(device->getCurrentSampleRate());
}

void Audio::audioDeviceStopped()
{
    DBG("Device Stopped");
}

void Audio::setGain(float newGain)
{
    sineOscillate.setAmplitude(newGain);
}

float Audio::getGain()
{
    float ampValue = amplitude.get();
    return ampValue;
}