/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) : audio (a)

{
    setSize (500, 400);
    
    startStopButton.setButtonText("Start");
    addAndMakeVisible(startStopButton);
    startStopButton.addListener(this);
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    startStopButton.setBounds(10, 10, getWidth() - 20, 40);
}




void MainComponent::buttonClicked(Button* button)
{    
    if (isRunning == true)
    {
        isRunning = false;
        counter.threadStart();
        startStopButton.setButtonText("Stop");
    }
    
    else if (isRunning == false)
    {
        isRunning = true;
        counter.threadStop();
        startStopButton.setButtonText("Start");
    }
}


//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Preferences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

