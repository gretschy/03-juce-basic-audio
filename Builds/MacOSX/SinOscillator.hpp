//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Tom on 03/11/2016.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class SinOscillator
{
public:
    
    SinOscillator();
    ~SinOscillator();
    
    float getSample();
    void setSample(float inputSampleRate);
    void setFrequency(float inputFreq);
    void setAmplitude(float inputAmp);
    
private:
    float frequency;
    float amplitude;
    float phasePosition;
    float phaseIncrement;
    float sampleRate;
};

#endif /* SinOscillator_hpp */