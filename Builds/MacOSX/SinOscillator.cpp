//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Tom on 03/11/2016.
//
//

#include "SinOscillator.hpp"

SinOscillator::SinOscillator()
{
    frequency = 440.f;
    phasePosition = 0.f;
}

SinOscillator::~SinOscillator()
{
    
}

void SinOscillator::setSample(float inputSampleRate)
{
    sampleRate = inputSampleRate;
}

float SinOscillator::getSample()
{
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency)/sampleRate;
    
        phasePosition+= phaseIncrement;
        
        if (phasePosition > twoPi)
        {
            phasePosition-= twoPi;
        }
        
        float sine;
        sine = sin(phasePosition);
    
    return sine * amplitude;
}

void SinOscillator::setFrequency(float inputFreq)
{
    frequency = inputFreq;
}

void SinOscillator::setAmplitude(float inputAmp)
{
    amplitude = inputAmp;
}

